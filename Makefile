test: phantomjs --ignore-ssl-errors=true
	@./test/page.js

build:
	@./node_modules/.bin/browserify -d lib/index.js -o assets/main.js

.PHONY: test build
