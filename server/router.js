var express = require('express');

var database = require('./database.js');

var router = express.Router();

router.get('/organizations', function (req, res) {
    var organizations = database.getOrganizations();

    res.json(organizations);
});

module.exports = router;
