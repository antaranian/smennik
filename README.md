# game-page

Headless...


## Installation

Using [npm](http://npmjs.org/):

    $ npm install spb25-game-page


## Usage

```js
var system = system  = require('system');

var GamePage = require('spb25-game-page');

var attrs = {
        id  : 'QmxP-24ar',
        name: 'Roma - Napoli',
        url : '#type=Coupon;key=15290827712C1_1_3;'
    };

var page = Page(attrs, { out: system.stdout });

page
    .open(function(err){
        if (err)
            throw new Error(err);

        console.log('Game ' + this.name + ' watching.')
    });
```


## Test

Run unit tests:

    $ make test


## License

The MIT License.
