var cheerio = require('cheerio');

var reduce  = require('lodash.reduce');

module.exports = function (html) {
    $ = cheerio.load(html);

    return reduce($('.items tbody tr'), parse, []);
};

module.exports.url = 'http://www.erbank.cz/ru/kurzovni-listek';



function parse (list, tr) {
    var tds = $('td', tr);

    var code = text(tds[0]).toLowerCase();

    var c = 10000 / text(tds[1]);

    list.push({
            code    : code,
            type    : 'cash',
            action  : 'buy',
            value   : value(tds[2], c)
        }, {
            code    : code,
            type    : 'cash',
            action  : 'sell',
            value   : value(tds[3], c)
        }, {
            code    : code,
            type    : 'noncash',
            action  : 'buy',
            value   : value(tds[5], c)
        }, {
            code    : code,
            type    : 'noncash',
            action  : 'sell',
            value   : value(tds[6], c)
        });

    return list;
}


function text (el) {
    return $(el).text().trim();
}


function value (el, c) {
    return (text(el).replace(',', '.') * c)
        .toFixed(2) * 1;
}
