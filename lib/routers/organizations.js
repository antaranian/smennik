var express = require('express');

var async = require('async');

var get = require('lodash.get');

var Rates         = require('../collections/rates.js'),
    Branches      = require('../collections/branches.js'),
    Оrganizations = require('../collections/organizations.js');

var router = express.Router();

router.get('/', function (req, res) {
    var rates         = new Rates(),
        branches      = new Branches(),
        organizations = new Оrganizations();

    async.waterfall([
        initialize,
        populate,
        aggregate
    ], done);

    function initialize (callback) {
        organizations.select(callback)
    }

    function populate (data, callback) {
        var ids = organizations.ids();

        async.each([rates, branches], supply, callback);

        function supply (ps, done) {
            ps.select({
                organizations: ids
            }, done);
        }

    }

    function aggregate (callback) {
        rates.forEach(function (rate) {
            organizations.get(rate.organization)
                .rates.add(rate);
        });

        branches.forEach(function (branch) {
            organizations.get(branch.organization)
                .branches.add(branch);
        });


        callback(null, organizations.serialize({
            props   : true,
            session : true,
            rates   : true,
            branches: true
        }));
    }

    function done (err, data) {
        if (err)
            return console.log(err);

        res.json(data);
    }

});

module.exports = router;
