var Model       = require('ampersand-model');

var pick        = require('lodash.pick'),
    keys        = require('lodash.keys'),
    values      = require('lodash.values');

var maria       = require('../database.js');

var op          = require('../utils/op.js');

var Rates       = require('../collections/rates.js'),
    Branches    = require('../collections/branches.js');

var Organization;

Organization = Model.extend({
    props: {
        id      : ['number', true],
        name    : ['string', true],
        link    : ['string', false],
        type    : ['string', true, 'bank'],
        slug    : ['string', true]
    },
    session: {
        size    : 'number'
    }
});

module.exports = Organization;

Organization.prototype.initialize = function (attrs, options) {
    if (typeof attrs == 'number')
        this.id = attrs;

    options = options || {};

    this.branches = new Branches([] || attrs.branches, {
        parent: this
    });

    this.rates = new Rates([] || attrs.rates, {
        parent: this,
        parser: options.parser
    });

    return this;
};


// CRUD functionality
// ------------------



Organization.prototype.save = function (data, done) {
    if (typeof data == 'function')
        done = data,
        data = null;

    done = op(done);

    if (data)
        this.set(data);

    var fn = this.isNew()
            ? this.insert
            : this.update;

    fn.call(this, callback.bind(this));

    function callback (err, res) {
        if (err)
            return done(err);

        if (this.isNew())
            this.id = res.insertId;

        done.call(this, null, this.toJSON());
    }

    return this;
};



/**
 * Update existing organization
 *
 * @param  {Function}       done    callback
 * @return {Organization}
 */

Organization.prototype.update = function (done) {
    var q = [
            'UPDATE',
                'organizations',
            'SET ?',
            'WHERE id = ?'
        ].join(' ');

    var data = this.serialize();

    maria.query(q, [data, this.id], done);

    return this;
};


Organization.prototype.insert = function (done) {
    var data = this.serialize();

    var q = [
            'INSERT INTO organizations',
                '(' + keys(data) + ')',
            'VALUES ?'
        ].join(' ');

    maria.query(q, [[values(data)]], done);

    return this;
};




Organization.prototype.serialize = function (options) {
    var data = this.getAttributes(options);

    console.log(data);

    if (options.rates)
        data.rates = this.rates.serialize();

    if (options.branches)
        data.branches = this.branches.serialize();

    return data;
};
