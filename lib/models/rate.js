var Model = require('ampersand-model');

var get = require('lodash.get');

var Rate;

Rate = Model.extend({
    props: {
        id      : 'number',
        code    : 'string',
        type    : 'string',
        action  : 'string',
        value   : 'number'
    },
    session : {
        organization: 'number'
    },
    derived: {
        ref: {
            deps: ['code', 'type', 'action'],
            fn  : function () {
                return [this.action, this.code, this.type]
                    .join('-');
            }
        }
    }
});

module.exports = Rate;

Rate.prototype.row = function () {
    return [
        this.organization || get(this, 'collection.parent.id'),
        this.code,
        this.type,
        this.action,
        this.value
    ];
};
