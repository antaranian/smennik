var Model = require('ampersand-model');

var omit    = require('lodash.omit'),
    keys    = require('lodash.keys'),
    values  = require('lodash.values'),
    get     = require('lodash.get');

var maria = require('../database.js');

var op = require('../utils/op.js');

var Branch;

Branch = Model.extend({
    props: {
        id              : 'number',
        name            : 'string',
        main            : 'number',
        email           : 'string',
        telephone       : 'string',
        faxnumber       : 'string',
        workinghours    : 'string',
        addressregion   : 'string',
        postalcode      : 'string',
        streetaddress   : 'string',
        lat             : ['number', true, 50],
        lng             : ['number', true, 14.5]
    },
    session : {
        organization: 'number'
    }
});

module.exports = Branch;


Branch.prototype.initialize = function (attrs, options) {
    if (typeof attrs == 'number')
        this.id = attrs;

    return this;
};

Branch.prototype.save = function (data, done) {
    if (typeof data == 'function')
        done = data,
        data = null;

    done = op(done);

    if (data)
        this.set(data);

    var fn = this.isNew()
            ? this.insert
            : this.update;

    fn.call(this, callback.bind(this));

    function callback (err, res) {
        if (err)
            return done(err);

        if (this.isNew())
            this.id = res.insertId;

        done.call(this, null, this.toJSON());
    }

    return this;
};


Branch.prototype.update = function (done) {
    var data = this.serialize();

    var q = [
            'UPDATE',
                'branches',
            'SET ?',
            'WHERE id = ?'
        ].join(' ');

    maria.query(q, [data, this.id], done);

    return this;
};


Branch.prototype.insert = function (done) {
    var data = this.serialize({ derived: true });

    var q = [
            'INSERT INTO branches',
                '(', keys(data) ,' )',
            'VALUES ?'
        ].join(' ');

    maria.query(q, [[values(data)]], done);

    return this;
};
