module.exports = function (fn) {
    return typeof fn == 'function' ? fn : noop;
}

function noop() {}
