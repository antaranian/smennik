var Collection  = require('ampersand-collection');

var lodashMixin = require('ampersand-collection-lodash-mixin');

var maria = require('../database.js');

var op = require('../utils/op.js');

var Organization = require('../models/organization.js');

var Organizations;

Organizations = Collection.extend(lodashMixin, {
    model: Organization,
    mainIndex: 'id',
    indexes: ['slug']
});

module.exports = Organizations;

Organizations.prototype.initialize = function (data, options) {

    return this;
};

Organizations.prototype.select = function (done) {
    done = op(done);

    var q =  [
            'SELECT',
                'organizations.*,',
                'COUNT(branches.id) AS size',
            'FROM organizations',
            'LEFT JOIN branches',
                'ON (organizations.id = branches.organization)',
            'GROUP BY',
                'organizations.id;'
        ].join('\n');

    maria.query(q, callback.bind(this));

    function callback (err, rows) {
        if (err)
            return done(err);

        this.set(rows);

        var data = this.serialize({
                session: true
            });

        done.call(this, null, data);
    }

    return this;
};


Organizations.prototype.delete = function (id, done) {
    if (!id)
        return done(new Error('No id'));

    done = op(done);

    // remove cached from memory
    var organization = this.get(id);

    if (organization)
        this.remove(organization);

    // remove from database
    var q = [
            'DELETE FROM organizations',
            'WHERE organizations.id = ?'
        ].join('\n');

    maria.query(q, [id], done);

    return this;
};


Organizations.prototype.insert = function (data, done) {
    this.add(data)
        .save(done);

    return this;
};


Organizations.prototype.update = function (id, data, done) {
    (this.get(id) || this.add({ id: id }))
        .save(data, done);

    return this;
};


Organizations.prototype.ids = function () {
    return this.map(function(organization){
        return organization.id;
    });
};


Organizations.prototype.serialize = function (options) {
    return this.map(function(organization) {
        return organization.serialize(options);
    });
};
