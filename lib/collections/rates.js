var Collection = require('ampersand-collection');

var request = require('superagent');

var lodashMixin = require('ampersand-collection-lodash-mixin');

var get         = require('lodash.get'),
    map         = require('lodash.map'),
    pick        = require('lodash.pick'),
    reduce      = require('lodash.reduce'),
    forEach     = require('lodash.foreach'),
    transform   = require('lodash.transform');

var maria   = require('../database.js');

var op  = require('../utils/op.js');

var Rate = require('../models/rate.js');

var Rates;

Rates = Collection.extend(lodashMixin, {
    model: Rate
});

module.exports = Rates;


Rates.prototype.initialize = function (data, options) {
    this.parser = (options || {}).parser || null;

    return this;
};


Rates.prototype.crawl = function (done) {
    done = op(done);

    var parse = this.parser;

    if (typeof parse != 'function')
        return done(new Error('No parser defined'));

    request
        .get(parse.url)
        .end(function(err, res){
            if (err)
                return done(err);

            var data;

            try {
                data = parse(res.text);
            }
            catch (err) {
                return done(err);
            }

            done(null, data);
        });

    return this;
};


Rates.prototype.select = function (options, done) {
    if (typeof options == 'function')
        done = options,
        options = null;

    done    = op(done);
    options = options || {};

    var organizations   = options.organizations || [get(this, 'parent.id')];

    var qi, qo;

    qi = [
        'SELECT',
            'action, code, max(timestamp) lasttime, type, organization',
        'FROM rates',
        'WHERE organization IN ?',
        'GROUP BY code, type, action, organization'
    ].join(' ');

    qo = [
        'SELECT f.*',
        'FROM (', qi, ') AS x',
        'INNER JOIN rates AS f',
        'ON  f.timestamp = x.lasttime',
        'AND f.code = x.code',
        'AND f.action = x.action',
        'AND f.type = x.type',
        'AND f.organization = x.organization',
    ].join(' ');

    maria.query(qo, [[organizations]], callback.bind(this));

    function callback (err, rows) {
        if (err)
            return done(err);

        this.reset(rows);

        var data = this.serialize({ session: true });

        return done.call(this, null, data);
    }

    return this;
};


Rates.prototype.update = function (data, done) {
    if (typeof data == 'function')
        done = data,
        data = null;

    done = op(done);

    var select  = this.select.bind(this),
        crawl   = this.crawl.bind(this),
        update  = this.update.bind(this),
        insert  = this.insert.bind(this);

    function callback (err) {
        if (err)
            return done(err);

        if (data)
            return insert(data, done);

        // if no input, crawl auto
        crawl(function (err, data) {
            if (err)
                return done(err);

            update(data, done);
        });
    }

    return select(callback);
};


Rates.prototype.amend = function (data) {
    var find    = this.find.bind(this),
        add     = this.add.bind(this),
        remove  = this.remove.bind(this);

    forEach(data, function (row) {
        var rate = find(pick(row, ['code', 'type', 'action']));

        if (!rate)
            return add(row);

        if (rate.value == row.value)
            return rate;

        remove(rate);

        return add(row);
    });

    return this;
};


Rates.prototype.insert = function (data, done) {
    if (data)
        this.amend(data);
    else
        done = data;

    done = op(done);

    var rows = [];

    this.reduce(function (memo, rate) {
        if (!rate.isNew())
            return memo;

        memo.push(rate.row());

        return memo;
    }, rows);

    if (!rows.length)
        return done(null, this.toJSON());

    var q = [
            'INSERT INTO rates',
                '( organization, code, type, action, value )',
            'VALUES ?'
        ].join(' ');

    maria.query(q, [rows], callback.bind(this));

    function callback (err, info) {
        if (err)
            return done(err);

        var data = this.serialize({ session: true });

        return done.call(this, null, data);
    }

    return this;
};


Rates.prototype.serialize = function (options) {
    return this.map(function(rate){
        return rate.serialize(options);
    });
};
