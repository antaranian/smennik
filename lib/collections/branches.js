var Collection  = require('ampersand-collection');

var request = require('superagent');

var lodashMixin = require('ampersand-collection-lodash-mixin');

var get         = require('lodash.get'),
    forEach     = require('lodash.foreach'),
    pick        = require('lodash.pick'),
    reduce      = require('lodash.reduce'),
    transform   = require('lodash.transform');

var maria   = require('../database.js');

var op  = require('../utils/op.js');

var Branch  = require('../models/branch.js');

var Branches;

Branches = Collection.extend(lodashMixin, {
    model: Branch
});

module.exports = Branches;


Branches.prototype.initialize = function (data, options) {

    return this;
};


Branches.prototype.select = function (options, done) {
    if (typeof options == 'function')
        done = options,
        options = null;

    done    = op(done);
    options = options || {};

    var organizations   = options.organizations || [get(this, 'parent.id')];


    var q = [
            'SELECT *',
            'FROM branches',
            'WHERE organization IN ?'
        ].join('\n');

    maria.query(q, [[organizations]], callback.bind(this));

    function callback (err, rows) {
        if (err)
            return done(err);

        this.set(rows);

        done.call(this, null, this.serialize({ session: true }));
    }

    return this;
};


Branches.prototype.delete = function (id, done) {
    if (!id)
        return done(new Error('No id'));

    done = op(done);

    // remove cached from memory
    var branch = this.get(id);

    if (branch)
        this.remove(branch);

    // remove from database
    var q = [
            'DELETE FROM branches',
            'WHERE id = ?'
        ].join('\n');

    maria.query(q, [id], done);

    return this;
};


Branches.prototype.insert = function (data, done) {
    this.add(data)
        .save(done);

    return this;
};


Branches.prototype.update = function (id, data, done) {
    (this.get(id) || this.add({ id: id }))
        .save(data, done);

    return this;
};




Branches.prototype.serialize = function (options) {
    return this.map(function(rate){
        return rate.serialize(options);
    });
};
