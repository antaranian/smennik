var express = require('express');

var hbs     = require('hbs'),
    serve   = require('serve-static');

var routers = {
        organizations: require('./lib/routers/organizations.js')
    };

var app = express();

app.set('view engine', 'html');
app.engine('html', hbs.__express);

app.use('/assets', serve(__dirname + '/assets'));
// app.use('/fonts', express.static('../fonts'));

app.get('/', function (req, res) {
    res.render('index.html');
});

app.listen(8085, function () {
    console.log('Example app listening on port 8080!');
});

app.use('/organizations/', routers.organizations);
